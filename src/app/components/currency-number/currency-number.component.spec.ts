import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CurrencyNumberComponent} from './currency-number.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('CurrencyNumberComponent', () => {
  let component: CurrencyNumberComponent;
  let fixture: ComponentFixture<CurrencyNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CurrencyNumberComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
