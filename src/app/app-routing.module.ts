import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ExerciseOneComponent} from './pages/exercise-one/exercise-one.component';
import {ExerciseTwoComponent} from './pages/exercise-two/exercise-two.component';
import {IndexComponent} from './pages/index/index.component';

const routes: Routes = [
  {
    path: 'index',
    component: IndexComponent
  },
  {
    path: 'exercise1',
    component: ExerciseOneComponent
  },
  {
    path: 'exercise2',
    component: ExerciseTwoComponent
  },
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
