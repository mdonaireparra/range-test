import { TestBed } from '@angular/core/testing';

import { RangeStubService } from './range-stub.service';

describe('RangeStubService', () => {
  let service: RangeStubService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RangeStubService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
