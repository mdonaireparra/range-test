import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

export const rangeNormalListFake: { min: number, max: number }[] = [
  {min: 1, max: 100},
  {min: 20, max: 40},
  {min: -10, max: 10},
  {min: 1.99, max: 5.99},
];

export const rangeFixedListFake: number[][] = [
  [1.99, 5.99, 10.99, 30.99, 50.99, 70.99],
  [-10, -5, 0, 20, 50, 70],
  [1000, 5000, 10000, 20000, 50000, 100000],
];

@Injectable({
  providedIn: 'root'
})
export class RangeStubService {

  constructor() {
  }

  getListRangeNormal(): Observable<{ min: number, max: number }[]> {
    return of(rangeNormalListFake);
  }

  getListRangeFixed(): Observable<number[][]> {
    return of(rangeFixedListFake);
  }
}
