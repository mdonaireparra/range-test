import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RangeComponent} from './components/range/range.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ExerciseOneComponent} from './pages/exercise-one/exercise-one.component';
import {ExerciseTwoComponent} from './pages/exercise-two/exercise-two.component';
import {CurrencyNumberComponent} from './components/currency-number/currency-number.component';
import { IndexComponent } from './pages/index/index.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    RangeComponent,
    ExerciseOneComponent,
    ExerciseTwoComponent,
    CurrencyNumberComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
