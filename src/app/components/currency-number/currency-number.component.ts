import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';

export enum TYPE_CURRENCY_NUMBER {
  MIN,
  MAX
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngc-currency-number',
  templateUrl: './currency-number.component.html',
  styleUrls: ['./currency-number.component.scss']
})
export class CurrencyNumberComponent implements OnInit {

  @Input() min = 1;
  @Input() max = 100;
  currencyMin = 1;
  currencyMax = 100;

  @Input() disabled = false;

  formGroup: FormGroup;
  editValue = false;

  typesCurrency = TYPE_CURRENCY_NUMBER;

  @Input('currencyMin')
  set UpdateCurrencyMin(currencyMin: number) {
    this.currencyMin = currencyMin;
    if (this.formGroup && this.typeCurrency === TYPE_CURRENCY_NUMBER.MIN && !this.disabled) {
      this.formGroup.setValue({numberValue: currencyMin});
    }
  }

  @Input('currencyMax')
  set UpdateCurrencyMax(currencyMax: number) {
    this.currencyMax = currencyMax;
    if (this.formGroup && this.typeCurrency === TYPE_CURRENCY_NUMBER.MAX && !this.disabled) {
      this.formGroup.setValue({numberValue: currencyMax});
    }
  }

  @Input() typeCurrency: TYPE_CURRENCY_NUMBER = TYPE_CURRENCY_NUMBER.MIN;
  @Output() changeCurrencyNumber = new EventEmitter<number>();

  constructor() {
    this.formGroup = new FormGroup(
      {
        numberValue: new FormControl(
          this.typeCurrency === TYPE_CURRENCY_NUMBER.MIN ? this.currencyMin : this.currencyMax, [
            Validators.required,
            this.minMaxValidator()
          ]
        )
      }
    );
  }

  ngOnInit(): void {
    this.formGroup.setValue({numberValue: this.typeCurrency === TYPE_CURRENCY_NUMBER.MIN ? this.currencyMin : this.currencyMax});
  }

  minMaxValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!(typeof control.value === 'number')) {
        return {notNumber: control.value};
      }
      const value = Number(control.value);
      if (value < this.min) {
        return {lessMin: control.value};
      }
      if (value > this.max) {
        return {greaterMax: control.value};
      }
      if (this.typeCurrency === TYPE_CURRENCY_NUMBER.MIN && this.currencyMax && value >= this.currencyMax) {
        return {collisionWithMax: control.value};
      }
      if (this.typeCurrency === TYPE_CURRENCY_NUMBER.MAX && value <= this.currencyMin) {
        return {collisionWithMin: control.value};
      }
      return null;
    };
  }

  onInputNumberChange(formGroup: FormGroup): void {
    if (formGroup.valid) {
      this.changeCurrencyNumber.emit(Number(formGroup.controls.numberValue.value));
      this.editValue = false;
    }
  }
}
