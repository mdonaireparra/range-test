import {Component} from '@angular/core';
import {RANGE_TYPES} from './components/range/range.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
