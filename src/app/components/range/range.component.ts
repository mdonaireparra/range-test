import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {TYPE_CURRENCY_NUMBER} from '../currency-number/currency-number.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

export enum RANGE_TYPES {
  FIXED = 'fixed',
  NORMAL = 'normal'
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngc-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RangeComponent),
      multi: true
    }
  ]
})
export class RangeComponent implements OnInit, AfterViewInit, ControlValueAccessor {

  @ViewChild('rangeSlider') rangeSlider!: ElementRef;

  rangeWidth = 0;
  rangeLeft = 0;

  dragMinPosition = 0;
  dragMaxPosition = 0;
  draggerBigger = 30;
  draggerSmall = 25;

  typesCurrency = TYPE_CURRENCY_NUMBER;
  typesRanges = RANGE_TYPES;

  @Input() min = 1;
  @Input() max = 100;
  @Input() type = RANGE_TYPES.NORMAL;
  @Input() values: number[] = [];
  currencyMin = 1;
  currencyMax = 100;

  @Output() rangeChange = new EventEmitter<{ min: number, max: number }>();

  isNgModel = false;
  onChange = (_: { min: number, max: number }) => {
  };
  onTouch = () => {
  };

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    this.initRangeMeasures(this.rangeSlider);
    this.initRangeValuesAndPositions(this.currencyMin, this.currencyMax);
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.initRangeMeasures(this.rangeSlider);
    setTimeout(() => {
      if (!this.isNgModel) {
        this.initRangeValuesAndPositions();
      }
    });
  }

  initRangeMeasures(rangeSlider: ElementRef): void {
    function getRangeLeft(el: any): number {
      let left = 0;
      if (el.offsetParent) {
        while (el.offsetParent) {
          left += el.offsetLeft;
          el = el.offsetParent;
        }
        return left;
      } else {
        return el.offsetLeft;
      }
    }

    this.rangeWidth = rangeSlider.nativeElement.offsetWidth;
    this.rangeLeft = getRangeLeft(rangeSlider.nativeElement);
  }

  writeValue(value: { min: number, max: number }): void {
    if (value && !!value.max && !!value.min) {
      this.isNgModel = true;
      this.initRangeValuesAndPositions(value.min, value.max);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }


  initRangeValuesAndPositions(currencyMin?: number, currencyMax?: number): void {
    if (this.type === RANGE_TYPES.FIXED) {
      this.initFixedRange(this.values, currencyMin, currencyMax);
    } else {
      this.initNormalRange(currencyMin, currencyMax);
    }
    this.dragMinPosition = this.getPositionInRange(this.currencyMin);
    this.dragMaxPosition = this.getPositionInRange(this.currencyMax);
  }

  initFixedRange(values: number[], currencyMin?: number, currencyMax?: number): void {
    if (!this.values.length) {
      throw new Error('Array of values are empty');
    }
    const indexMin = currencyMin ? values.findIndex(value => value === currencyMin) : 0;
    const indexMax = currencyMax ? values.findIndex(value => value === currencyMax) : values.length - 1;
    if (indexMin < 0 || indexMax < 0) {
      throw new Error('NgModel min or max not exist in array');
    }

    this.currencyMin = this.values[indexMin];
    this.currencyMax = this.values[indexMax];
  }

  private initNormalRange(currencyMin: number | undefined, currencyMax: number | undefined): void {
    if (this.min >= this.max) {
      throw new Error('Min are greater or equal max in Input');
    }

    if ((!!currencyMin && currencyMin < this.min) || (!!currencyMax && currencyMax > this.max)) {
      throw new Error('Min Input are greater NgModel min or max Input are greater NgModel max');
    }

    if (!!currencyMin && !!currencyMax && currencyMin >= currencyMax) {
      throw new Error('NgModel min are greater or equal max!');
    }
    this.currencyMin = currencyMin || this.min;
    this.currencyMax = currencyMax || this.max;
  }

  onDragMin($event: DragEvent): void {
    const actualMin = this.getCurrentValueFromPosition($event);
    if ($event.pageX >= (this.rangeLeft - this.draggerBigger) && $event.pageX <=
      (this.rangeLeft + this.rangeWidth + this.draggerBigger)) {
      if (this.type === RANGE_TYPES.FIXED) {
        this.onDragMinWithFixed($event, actualMin);
      } else if (actualMin < this.currencyMax && actualMin >= this.min) {
        this.currencyMin = actualMin;
        this.dragMinPosition = $event.pageX - this.rangeLeft - this.draggerBigger;
        this.emitCurrentValues(this.currencyMin, this.currencyMax);
      } else {
        $event.stopImmediatePropagation();
      }
    } else {
      $event.stopImmediatePropagation();
    }
  }

  onDragMinWithFixed($event: DragEvent, actualMin: number): void {
    if (!actualMin || actualMin >= this.currencyMax) {
      $event.stopImmediatePropagation();
    } else if (actualMin !== this.currencyMin) {
      this.currencyMin = actualMin;
      this.dragMinPosition = this.getPositionInRange(actualMin) - this.draggerSmall;
      this.emitCurrentValues(this.currencyMin, this.currencyMax);
    }
  }

  onDragMax($event: DragEvent): void {
    const actualMax = this.getCurrentValueFromPosition($event);
    if ($event.pageX >= (this.rangeLeft - this.draggerBigger) && $event.pageX <=
      (this.rangeLeft + this.rangeWidth + this.draggerBigger)) {
      if (this.type === RANGE_TYPES.FIXED) {
        this.onDragMaxWithFixed($event, actualMax);
      } else if (actualMax > this.currencyMin && actualMax <= this.max) {
        this.currencyMax = actualMax;
        this.dragMaxPosition = $event.pageX - this.rangeLeft - this.draggerBigger;
        this.emitCurrentValues(this.currencyMin, this.currencyMax);
      } else {
        $event.stopImmediatePropagation();
      }
    } else {
      $event.stopImmediatePropagation();
    }
  }

  onDragMaxWithFixed($event: DragEvent, actualMax: number): void {
    if (!actualMax || actualMax <= this.currencyMin) {
      $event.stopImmediatePropagation();
    } else if (actualMax !== this.currencyMax) {
      this.currencyMax = actualMax;
      this.dragMaxPosition = this.getPositionInRange(actualMax);
      this.emitCurrentValues(this.currencyMin, this.currencyMax);
    }
  }

  private getCurrentValueFromPosition($event: DragEvent): number {
    const positionInRange = $event.pageX - this.rangeLeft - (this.draggerSmall);
    if (this.type === RANGE_TYPES.FIXED) {
      const jump = this.rangeWidth / (this.values.length - 1);
      const indexActualValue = Math.floor(positionInRange / jump);
      return this.values[indexActualValue];
    } else {
      return this.min + Math.floor(positionInRange * (this.max - this.min) / this.rangeWidth);
    }
  }

  private getPositionInRange(currentValue: number): number {
    if (this.type === RANGE_TYPES.FIXED) {
      const indexValue = this.values.findIndex(value => value === currentValue);
      const jump = this.rangeWidth / (this.values.length - 1);
      return (jump * indexValue);
    } else {
      return ((this.rangeWidth * (currentValue - this.min)) / (this.max - this.min));
    }
  }

  onChangeMinCurrencyValue(currencyMin: number): void {
    this.currencyMin = currencyMin;
    this.dragMinPosition = this.getPositionInRange(currencyMin) - this.draggerSmall;
    this.emitCurrentValues(currencyMin, this.currencyMax);
  }

  onChangeMaxCurrencyValue(currencyMax: number): void {
    this.currencyMax = currencyMax;
    this.dragMaxPosition = this.getPositionInRange(currencyMax);
    this.emitCurrentValues(this.currencyMin, currencyMax);
  }

  emitCurrentValues(currencyMin: number, currencyMax: number): void {
    if (!this.isNgModel) {
      this.rangeChange.emit({min: currencyMin, max: currencyMax});
    } else {
      this.onChange({min: currencyMin, max: currencyMax});
      this.onTouch();
    }
  }

}
