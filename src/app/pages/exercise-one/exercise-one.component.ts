import {Component, OnInit} from '@angular/core';
import {RangeStubService} from '../../services/range-stub.service';

@Component({
  selector: 'app-exercise-one',
  templateUrl: './exercise-one.component.html',
  styleUrls: ['./exercise-one.component.scss']
})
export class ExerciseOneComponent implements OnInit {

  rangeNormalList: { min: number, max: number }[] = [];
  arrayValues: { min: number, max: number }[] = [];

  itemNgModel!: { min: number, max: number };

  constructor(private rangeStubService: RangeStubService) {
  }

  ngOnInit(): void {
    this.rangeStubService.getListRangeNormal().subscribe(
      list => {
        this.rangeNormalList = list;
        this.itemNgModel = list[0];
        this.arrayValues = new Array(list.length).fill({min: null, max: null});
      }
    );
  }

  onChanges($event: any, indexList: number): void {
    this.arrayValues.splice(indexList, 1, $event);
  }

}
