import {Component, OnInit} from '@angular/core';
import {RANGE_TYPES} from '../../components/range/range.component';
import {RangeStubService} from '../../services/range-stub.service';

@Component({
  selector: 'app-exercise-two',
  templateUrl: './exercise-two.component.html',
  styleUrls: ['./exercise-two.component.scss']
})
export class ExerciseTwoComponent implements OnInit {

  rangeTypes = RANGE_TYPES;

  rangeFixedList: number[][] = [];
  arrayValues: { min: number, max: number }[] = [];

  itemNgModel!: { min: number, max: number };

  constructor(private rangeStubService: RangeStubService) {
  }

  ngOnInit(): void {
    this.rangeStubService.getListRangeFixed().subscribe(
      list => {
        this.rangeFixedList = list;
        this.itemNgModel = {min: list[0][0], max: list[0][2]};
        this.arrayValues = new Array(list.length).fill({currentMin: null, currentMax: null});
      }
    );
  }

  onChanges($event: any, indexList: number): void {
    this.arrayValues.splice(indexList, 1, $event);
  }

}
