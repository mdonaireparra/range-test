import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {RANGE_TYPES, RangeComponent} from './range.component';
import {rangeFixedListFake} from '../../services/range-stub.service';
import {CurrencyNumberComponent} from '../currency-number/currency-number.component';
import {FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, forwardRef} from '@angular/core';

describe('RangeComponent', () => {
  let component: RangeComponent;
  let fixture: ComponentFixture<RangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        RangeComponent,
        CurrencyNumberComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => RangeComponent),
          multi: true
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create fixed range', () => {
    component.type = RANGE_TYPES.FIXED;
    component.values = [...rangeFixedListFake[0]];
    expect(component).toBeTruthy();
  });

  it('should create normal range', () => {
    component.type = RANGE_TYPES.NORMAL;
    component.min = 0;
    component.max = 100;
    expect(component).toBeTruthy();
  });

  it('Array empty in range fixed throw Error', fakeAsync(() => {
    component.type = RANGE_TYPES.FIXED;
    component.values = [];
    expect(() => {
      component.initRangeValuesAndPositions();
    }).toThrowError('Array of values are empty');
  }));

  it('NgModel min and max not exist in  range fixed and throw Error', fakeAsync(() => {
    component.type = RANGE_TYPES.FIXED;
    component.values = rangeFixedListFake[0];
    expect(() => {
      component.initRangeValuesAndPositions(1, 100);
    }).toThrowError('NgModel min or max not exist in array');
  }));

  it('min > max Inputs in range normal throw Error', fakeAsync(() => {
    component.type = RANGE_TYPES.NORMAL;
    component.max = 0;
    component.min = 100;
    expect(() => {
      component.initRangeValuesAndPositions();
    }).toThrowError('Min are greater or equal max in Input');
  }));

  it('NgModel max > max in Input range normal throw Error', fakeAsync(() => {
    component.type = RANGE_TYPES.NORMAL;
    component.min = 0;
    component.max = 120;
    expect(() => {
      component.initRangeValuesAndPositions(3, 133);
    }).toThrowError('Min Input are greater NgModel min or max Input are greater NgModel max');
  }));

  it('ngModel min < min Input in range normal throw Error', fakeAsync(() => {
    component.type = RANGE_TYPES.NORMAL;
    component.min = 1;
    component.max = 100;
    expect(() => {
      component.initRangeValuesAndPositions(-1, 50);
    }).toThrowError('Min Input are greater NgModel min or max Input are greater NgModel max');
  }));
});
